#!/bin/sh

cp -rf . ~/qmk_firmware/keyboards/keychron/q2/ansi_encoder/
qmk flash -kb keychron/q2/ansi_encoder -km mbednarek360
mv ~/qmk_firmware/keychron_q2_ansi_encoder_mbednarek360.bin firmware.bin
